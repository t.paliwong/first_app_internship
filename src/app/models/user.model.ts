export interface User {
  id: number;
  fullname: string;
  birthday: string;
  addresses: Address[];
  gender: string;
  marital_status: string;
  gender_id: number;
  marital_status_id: number;
}

interface Address {
  name: string;
}
