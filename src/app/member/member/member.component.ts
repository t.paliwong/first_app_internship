import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from '../../models/user.model';
enum Gender {
  Men = '1',
  Woman = '2',
}

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss'],
})
export class MemberComponent implements OnInit {
  users: Observable<User[]>;
  gender = Gender;
  constructor(private service: UserService) {}

  ngOnInit(): void {
    this.getUser();
  }

  getUser() {
    this.users = this.service.getUser();
  }
}
