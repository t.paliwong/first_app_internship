import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
enum Gender {
  Men = '1',
  Woman = '2',
}

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.scss'],
})
export class MemberDetailComponent implements OnInit {
  id: number;
  gender = Gender;
  user: User;
  constructor(
    private route: ActivatedRoute,
    private service: UserService,
    private location: Location
  ) {
    this.id = this.route.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.getUser();
  }

  getUser() {
    this.service.getUserById(this.id).subscribe((user) => (this.user = user));
  }

  backClicked() {
    this.location.back();
  }
}
