import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MemberRoutingModule } from './member-routing.module';
import { MemberComponent } from './member/member.component';
import { MemberDetailComponent } from './member-detail/member-detail.component';
import { PipeModule } from '../pipe/pipe.module';

@NgModule({
  declarations: [MemberComponent, MemberDetailComponent],
  imports: [CommonModule, MemberRoutingModule, PipeModule],
})
export class MemberModule {}
