export const gender = [
  { id: 1, name: 'ชาย' },
  { id: 2, name: 'หญิง' },
];

export const marital_status = [
  { id: 1, value: 'โสด' },
  { id: 2, value: 'มีแฟนแล้ว' },
  { id: 3, value: 'แต่งงานแล้ว' },
];
