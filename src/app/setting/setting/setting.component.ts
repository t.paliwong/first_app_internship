import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
} from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from '../../models/user.model';
import { DialogAddComponent } from '../../components/dialog-add/dialog-add.component';
import { DialogEditComponent } from '../../components/dialog-edit/dialog-edit.component';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
})
export class SettingComponent implements OnInit {
  users: User[];
  search: FormControl = new FormControl('');

  constructor(private service: UserService, public dialog: MatDialog) {
    //search term
    this.search.valueChanges
      .pipe(
        map((search) => search.trim()),
        filter((search) => search.length > 1),
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((search) => {
        service.getUser(search).subscribe((resp) => (this.users = resp));
      });
  }

  getUser() {
    this.service.getUser().subscribe((user) => (this.users = user));
  }

  ngOnInit(): void {
    this.getUser();
  }

  showModalDialog() {
    const dialogRef = this.dialog.open(DialogAddComponent);
    dialogRef.afterClosed().subscribe((result) => {
      if (result?.event === 'Success') this.getUser();
    });
  }

  onDelete(id: number) {
    this.service.deleteUser(id).subscribe(() => this.getUser());
  }

  onEdit(id: number) {
    const dialogRef = this.dialog.open(DialogEditComponent, {
      data: id,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result?.event === 'Success') this.getUser();
    });
  }

  handleSearch() {
    if (this.search.value.length < 1) {
      this.getUser();
      return;
    }

    // this.search.valueChanges
    //   .pipe(
    //     // map((search) => search.trim()),
    //     // filter((search) => search.length > 2),
    //     debounceTime(500),
    //     distinctUntilChanged()
    //     // switchMap((search) => this.service.getUser(search))
    //   )
    //   .subscribe((dataValue) => {
    //     console.log('dataValue', dataValue);
    //   });
  }
}
