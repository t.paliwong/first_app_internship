import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dialog-add',
  templateUrl: './dialog-add.component.html',
  styleUrls: ['./dialog-add.component.scss'],
})
export class DialogAddComponent implements OnInit {
  constructor(
    private dialogRef: MatDialogRef<DialogAddComponent>,
    private service: UserService
  ) {}

  ngOnInit(): void {}

  onSubmit(user: User) {
    this.service
      .addUser(user)
      .subscribe(() => this.dialogRef.close({ event: 'Success' }));
  }
}
