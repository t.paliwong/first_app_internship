import { Component, OnInit, Inject } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dialog-edit',
  templateUrl: './dialog-edit.component.html',
  styleUrls: ['./dialog-edit.component.scss'],
})
export class DialogEditComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public id: number,
    private service: UserService,
    private dialogRef: MatDialogRef<DialogEditComponent>
  ) {}

  ngOnInit(): void {}

  onSubmit(user: User) {
    user.id = this.id;
    this.service
      .updateUser(user)
      .subscribe(() => this.dialogRef.close({ event: 'Success' }));
    // this.dialogRef.close({ event: 'Success' });
  }
}
