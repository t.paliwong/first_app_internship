import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { gender } from '../../options/index';
import { DialogAddComponent } from '../dialog-add/dialog-add.component';
import { DialogEditComponent } from '../dialog-edit/dialog-edit.component';

interface Gender {
  id: number;
  name: string;
}
@Component({
  selector: 'app-add-edit-form',
  templateUrl: './add-edit-form.component.html',
  styleUrls: ['./add-edit-form.component.scss'],
})
export class AddEditFormComponent implements OnInit {
  @Input() type: string; // addUser editUser
  @Input() set id(id: number) {
    this.userService.getUserById(id).subscribe((user) => {
      if (user.addresses?.length > 0) {
        for (let address of user.addresses) {
          this.addAddress();
        }
      }
      this.checkoutForm.patchValue(user);
    });
  }
  @Output() response = new EventEmitter<User>();

  checkoutForm: FormGroup = this.formBuilder.group({
    fullname: [
      null,
      [Validators.required, Validators.pattern('[a-zA-Zก-๙ ]{0,20}')],
    ],
    gender_id: [null, Validators.required],
    addresses: this.formBuilder.array([]),
    birthday: ['', Validators.required],
    marital_status_id: ['', Validators.required],
  });
  submitted: boolean = false;
  gender: Gender[] = gender;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    public dialogAdd: MatDialogRef<DialogAddComponent>,
    public dialogEdit: MatDialogRef<DialogEditComponent>
  ) {}

  ngOnInit(): void {}

  get f(): { [key: string]: AbstractControl } {
    return this.checkoutForm.controls;
  }

  get address() {
    return this.checkoutForm.controls['addresses'] as FormArray;
  }

  addAddress() {
    const addressForm = this.formBuilder.group({
      name: [null, Validators.required],
    });
    this.address.push(addressForm);
  }

  deleteAddress(lessonIndex: number) {
    this.address.removeAt(lessonIndex);
  }

  onSubmit() {
    this.submitted = true;
    if (this.checkoutForm.invalid) return;

    // console.log(this.checkoutForm.value)
    this.response.emit(this.checkoutForm.value);
  }
}
