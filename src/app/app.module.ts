import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AccordionModule } from 'primeng/accordion';
import { DialogModule } from 'primeng/dialog';
import { AppComponent } from './app.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogAddComponent } from './components/dialog-add/dialog-add.component';
import { DialogEditComponent } from './components/dialog-edit/dialog-edit.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AddEditFormComponent } from './components/add-edit-form/add-edit-form.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { MemberModule } from './member/member.module';
import { SettingModule } from './setting/setting.module';
import { TestComponent } from './components/test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    DialogAddComponent,
    DialogEditComponent,
    NavbarComponent,
    AddEditFormComponent,
    MainLayoutComponent,
    TestComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AccordionModule,
    DialogModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MemberModule,
    SettingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
