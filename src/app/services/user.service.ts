import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  apiUrl: string = environment.apiUrl+"/users";
  constructor(private http: HttpClient) {}

  getUser(fullname?: string): Observable<User[]> {
    let API_URL: string = this.apiUrl;
    if (fullname) API_URL = API_URL + '?fullname=' + fullname;
    return this.http.get<User[]>(API_URL);
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(`${this.apiUrl}/${id}`);
  }

  addUser(data: User) {
    return this.http.post(`${this.apiUrl}`, { ...data });
  }

  deleteUser(id: number) {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }

  updateUser(user: User) {
    const { id } = user;
    return this.http.put(`${this.apiUrl}/${id}`, { ...user });
  }
}
